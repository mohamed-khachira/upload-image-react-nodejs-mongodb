import "./App.css";
import ImageUploadComponent from "./components/ImageUploadComponent";

function App() {
  return (
    <div className="App">
      <ImageUploadComponent />
    </div>
  );
}

export default App;
