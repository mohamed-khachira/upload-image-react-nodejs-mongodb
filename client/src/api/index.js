import axios from "axios";

const baseURL = process.env.REACT_APP_BASE_URL;

const api = axios.create({
  baseURL,
});

export const AddImage = (payload, config) =>
  api.post(`/image`, payload, config);

const apis = {
  AddImage,
};

export default apis;
