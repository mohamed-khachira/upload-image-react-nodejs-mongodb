import React, { useState } from "react";
import api from "../api";
import DefaultImg from "../assets/default-img.jpg";
import Alert from "./Alert";

function ImageUploadComponent() {
  const [selectedImage, setSelectedImage] = useState(null);
  const [imageInputState, setImageInputState] = useState("");
  const [image, setImage] = useState(DefaultImg);
  const [successMsg, setSuccessMsg] = useState("");
  const [errMsg, setErrMsg] = useState("");
  const [loaded, setloaded] = useState(0);

  const onChangeHandler = (event) => {
    if (checkMimeType(event)) {
      // if return true allow to setState
      setSelectedImage(event.target.files[0]);
      setImageInputState(event.target.value);
    }
  };

  const checkMimeType = (event) => {
    // list allow mime type
    const types = ["image/png", "image/jpeg", "image/jpg"];

    //getting file object
    let file = event.target.files[0];

    // compare file type find doesn't matach
    if (types.every((type) => file.type !== type)) {
      event.target.value = null;
      setErrMsg(file.type + " is not a supported format");
      return false;
    }
    return true;
  };
  const onClickHandler = async () => {
    if (!imageInputState) {
      setErrMsg("Image Required!");
      return false;
    }
    const data = new FormData();
    data.append("myImage", selectedImage);
    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
      onUploadProgress: (ProgressEvent) => {
        setloaded((ProgressEvent.loaded / ProgressEvent.total) * 100);
      },
    };
    try {
      const addedImage = await api.AddImage(data, config);
      setImage(addedImage.data.imagePath);
      setSelectedImage(null);
      setImageInputState("");
      setSuccessMsg("Image uploaded successfully");
      setErrMsg("");
    } catch (error) {
      console.log(error);
      setErrMsg("Something went wrong!");
    }
  };
  return (
    <div className="container">
      <h2 className="heading">Custom React Image Upload</h2>
      <div className="image-container">
        <Alert msg={errMsg} type="danger" />
        <Alert msg={successMsg} type="success" />
        <input
          className="img-input"
          type="file"
          name="myImage"
          value={imageInputState}
          onChange={onChangeHandler}
        />
        <button
          className="button-primary"
          type="button"
          onClick={onClickHandler}
        >
          Upload
        </button>
      </div>
      <div className="row">
        <img src={image} alt="Preview" className="process__image" />
      </div>
    </div>
  );
}

export default ImageUploadComponent;
