import React from "react";

function Alert({ msg, type }) {
  return <>{msg && <div className={`alert alert-${type}`}>{msg}</div>}</>;
}

export default Alert;
