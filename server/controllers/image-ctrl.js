// Image model
const Image = require("../models/image");
const multer = require("multer");
const { v4: uuidv4 } = require("uuid");

// destination folder
const DIR = "./public/uploads/";

// Create a multer instance and set the destination folder and file name
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, DIR);
  },
  filename: function (req, file, cb) {
    cb(null, uuidv4() + Date.now());
  },
});

// define the file types which are to be accepted by the server
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype == "image/png" ||
    file.mimetype == "image/jpg" ||
    file.mimetype == "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
    return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
  }
};

// Create an upload instance and receive a single file
const upload = multer({
  storage: storage,
  fileFilter: fileFilter,
}).single("myImage");

// upload and store image to MongoDB
uploadImage = (req, res) => {
  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return res.status(500).json(err);
    } else if (err) {
      return res.status(500).json(err);
    }
    const url = req.protocol + "://" + req.get("host");
    const newImage = new Image({
      imageName: req.file.filename,
    });
    newImage
      .save()
      .then((result) => {
        res.status(201).json({
          success: true,
          id: newImage._id,
          message: "Image added successfully!",
          imagePath: url + "/uploads/" + req.file.filename,
        });
      })
      .catch((err) => {
        res.status(500).json({
          error: err,
        });
      });
  });
};

module.exports = {
  uploadImage,
};
