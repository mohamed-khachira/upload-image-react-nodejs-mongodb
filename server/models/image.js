const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/* 
  Image Schema for storing images in the  mongodb database
*/
const ImageSchema = new Schema(
  {
    imageName: { type: String, required: true },
  },
  { timestamps: true },
  {
    collection: "Images",
  }
);

module.exports = mongoose.model("Image", ImageSchema);
