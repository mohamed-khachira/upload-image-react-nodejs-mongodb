const express = require("express");

const ImageCtrl = require("../controllers/image-ctrl");

const router = express.Router();

// Setup the POST route to upload a file
router.post("/image", ImageCtrl.uploadImage);

module.exports = router;
