# Frontend Developer Interview Stage 1

Develop a simple app with image uploading to the server.

![](task_upload.png)

## Tools

- React
- NodeJS
- Express
- MongoDB

## Requirements

- User will be able to upload an image (no other file extension)
- File will be uploaded to backend server (it can be local)
- File name should be UIUD (unique ID + timestamp)
- User will get feedback upon completion
- Add a record to mongoDB instance (local) or any other database with metadata (timestamp, file name)

## Installation with docker & docker-compose

1- Clone Repository from GitLab

```sh
git clone https://gitlab.com/mohamed-khachira/upload-image-react-nodejs-mongodb.git
```

2- Move into the folder && Start containers

#### Backend (build container)

```sh
cd server
make build
```

#### Frontend (build container)

```sh
cd client
make build
```

#### Run all container

```sh
cd ..
make run
```

| App url                | Api url                   |
| ---------------------- | ------------------------- |
| http://localhost:3000/ | http://localhost:5000/api |
